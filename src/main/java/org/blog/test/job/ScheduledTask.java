package org.blog.test.job;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ScheduledTask {

    private static final SimpleDateFormat dateformat = new SimpleDateFormat("HH:mm:ss");

    // Excuted every 5, 10 seconds
    @Scheduled(cron = "*/5 * * * * *")
    public void report5Time() {
        System.out.println("every 5 second " + dateformat.format(new Date()));
    }

    // Excuted every 2,4,6,8,10 seconds
    @Scheduled(cron = "*/2 * * * * *")
    public void report2Time() {
        System.out.println("every 2 second " + dateformat.format(new Date()));
    }

    // Excuted every day 14:15:00
    @Scheduled(cron = "0 15 14 * * *")
    public void reportSpecificTime() {
        System.out.println("14:15:00 =>" + dateformat.format(new Date()));
    }
}
